# 3D_mask_rotator.py

Rotate a 2d mask from a FITS file to create a 3d surface


Usage: 


> python3 mask_rotator_3d.py masks_t_039-0_2011-05-11T02\:25\:55.840.fits result.stl




Function Definitions:

calc_mass_center: Calculates the center of mass of a 2D dataset

load_fits: Loads data from a FITS file 

plot_sphere: Plots a simple wireframe sphere on a given 3D axis

crop_fragment: Extracts a circular region from a 2D array based on a specified center and radius

calculate_cropped_line_coordinates: Adjusts line segment coordinates to fit within a specified circular crop area

rotate_2d_fragment: Rotates a 2D array fragment around a specified line segment, creating multiple instances to represent a 3D rotation

approximate_ellipse_2d: Uses Principal Component Analysis (PCA) to fit an ellipse to a set of 2D points.
ellipse_to_ellipsoid_params: Converts 2D ellipse parameters into 3D ellipsoid parameters

plot_sphere_and_ellipsoid: Visualizes both a sphere and an ellipsoid in a 3D plot

insert_3d_fragment: Inserts a 3D fragment into a larger 3D array at specified coordinates

plot_3d_array_and_sphere: Visualizes a 3D array and overlays a transparent sphere

ellipsoid_fit and ellipsoid_fit_list: Perform an ellipsoid fitting to 3D data points using a least squares method.
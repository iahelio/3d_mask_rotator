import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from stl import mesh

import numpy as np
from scipy.spatial.transform import Rotation as R
from scipy.interpolate import griddata

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

from scipy.optimize import minimize
from scipy.optimize import least_squares
#from mayavi import mlab
#import vtk



def calc_mass_center(data, inc=1):
    x_cent, y_cent, m_s = 0, 0, 0
    x_n, y_n = len(data[0]), len(data)
    
    for x in range(1, x_n, inc):
        for y in range(1, y_n, inc):
            data_xy = data[y][x]  # Ensure correct indexing
            if np.isnan(data_xy):
                print('Warning: NaN encountered')
                data_xy = 0
            x_cent += x * data_xy
            y_cent += y * data_xy
            m_s += data_xy
            
    if m_s == 0:
        print('Warning: Mass sum is zero')
        return 0, 0
    
    return int(round(x_cent / m_s)), int(round(y_cent / m_s))

def load_fits(file_path):
    with fits.open(file_path) as hdul:
        data = hdul[0].data
        header = hdul[0].header
    return data, header

def plot_sphere(ax, center, radius):
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x = center[0] + radius * np.cos(u) * np.sin(v)
    y = center[1] + radius * np.sin(u) * np.sin(v)
    z = center[2] + radius * np.cos(v)
    ax.plot_wireframe(x, y, z, color="b", alpha=0.1)



def crop_fragment(data, x, y, radius):
    """
    Extract a circular fragment from the data array centered at (x, y) with the given radius.

    Args:
    - data: 2D numpy array from which to extract the fragment.
    - x, y: Center coordinates of the circular fragment.
    - radius: Radius of the circular fragment.

    Returns:
    - fragment: Extracted fragment as a new 2D numpy array.
    """
    # Get the dimensions of the data array
    height, width = data.shape

    # Create an open grid with x and y coordinates
    y_grid, x_grid = np.ogrid[:height, :width]

    # Calculate the distance of each point from the center (x, y)
    dist_from_center = np.sqrt((x_grid - x)**2 + (y_grid - y)**2)

    # Create a mask for points within the specified radius from the center
    mask = dist_from_center <= radius

    # Initialize an empty array with the same shape as the data
    fragment = np.zeros_like(data)

    # Apply the mask to extract the fragment
    fragment[mask] = data[mask]

    # Optionally, you might want to return only the bounding box that contains the circle,
    # to avoid having a large array mostly filled with zeros.
    # Find the bounding box of the mask
    rows = np.any(mask, axis=1)
    cols = np.any(mask, axis=0)
    ymin, ymax = np.where(rows)[0][[0, -1]]
    xmin, xmax = np.where(cols)[0][[0, -1]]

    # Extract the bounding box
    cropped_fragment = fragment[ymin:ymax+1, xmin:xmax+1]

    return cropped_fragment


def calculate_cropped_line_coordinates(x1, y1, x2, y2, crop_x, crop_y, radius):
    """
    Calculate the coordinates of a line segment's endpoints within a cropped circular fragment.

    Args:
    - x1, y1: Original x and y coordinates of the first endpoint of the line segment.
    - x2, y2: Original x and y coordinates of the second endpoint of the line segment.
    - crop_x, crop_y: Center coordinates of the circular crop.
    - radius: Radius of the circular crop.

    Returns:
    - (x1_c, y1_c): Coordinates of the first endpoint within the cropped fragment.
    - (x2_c, y2_c): Coordinates of the second endpoint within the cropped fragment.
    """
    # Calculate the bounding box of the circular crop
    xmin, ymin = crop_x - radius, crop_y - radius
    xmax, ymax = crop_x + radius, crop_y + radius

    # Translate the original coordinates into the space of the cropped fragment
    x1_c, y1_c = x1 - xmin, y1 - ymin
    x2_c, y2_c = x2 - xmin, y2 - ymin

    return x1_c, y1_c, x2_c, y2_c


def rotate_2d_fragment(data_2d, x1_c, y1_c, x2_c, y2_c, num_steps=36):
    # Define the 3D rotation axis based on the given points, assuming Z=0 for 2D data
    axis_start = np.array([x1_c, y1_c, 0])
    axis_end = np.array([x2_c, y2_c, 0])
    rotation_axis = axis_end - axis_start

    # Calculate rotation step size
    angle_step = 360 / num_steps

    # Placeholder for storing all rotated instances
    rotated_fragments = []

    for step in range(num_steps):
        angle = step * angle_step
        rotation = R.from_rotvec(rotation_axis * np.radians(angle))

        # Rotate each point in the feature
        for i in range(data_2d.shape[0]):
            for j in range(data_2d.shape[1]):
                # Convert 2D point to 3D
                point_3d = np.array([i, j, 0])
                # Apply rotation
                rotated_point = rotation.apply(point_3d)
                rotated_fragments.append((rotated_point, data_2d[i, j]))

    # Determine the bounds of the rotated fragments
    all_points = np.array([point for point, value in rotated_fragments])
    min_bounds = np.min(all_points, axis=0)
    max_bounds = np.max(all_points, axis=0)
    data_3d_size = np.ceil(max_bounds - min_bounds).astype(int) + 1  # Add 1 to ensure bounds are inclusive

    # Create the 3D array and fill in the rotated fragments
    data_3d = np.zeros(data_3d_size)
    for point, value in rotated_fragments:
        # Adjust points to fit in the data_3d array
        adjusted_point = np.round(point - min_bounds).astype(int)
        # Ensure indices are within the bounds of the array
        adjusted_point = np.clip(adjusted_point, 0, data_3d_size - 1)
        data_3d[tuple(adjusted_point)] = value

    return data_3d




def approximate_ellipse_2d(data_2d):
    # Extract coordinates of non-zero points
    y_coords, x_coords = np.nonzero(data_2d)
    points = np.vstack([x_coords, y_coords]).T  # Shape: (n_points, 2)

    # Perform PCA
    pca = PCA(n_components=2)
    pca.fit(points)

    # The center of the ellipse is the mean of the points
    center = pca.mean_

    # The lengths of the axes are proportional to the square root of the eigenvalues
    # (scaled by some factor depending on how you define the "radius" in your context)
    radii = np.sqrt(pca.explained_variance_)

    # Orientation of the ellipse
    angle = np.arctan2(pca.components_[0, 1], pca.components_[0, 0])

    return {
        'center_x': center[0],
        'center_y': center[1],
        'major_axis_length': max(radii),
        'minor_axis_length': min(radii),
        'angle': np.degrees(angle)  # Convert radians to degrees
    }




def ellipse_to_ellipsoid_params(ellipse_params, A, B):
    # Extract ellipse parameters
    center_x = ellipse_params['center_x']
    center_y = ellipse_params['center_y']
    a = ellipse_params['major_axis_length']  # Semi-major axis length
    b = ellipse_params['minor_axis_length']  # Semi-minor axis length
    angle = ellipse_params['angle']  # Orientation angle of the ellipse

    # Since we're rotating around the major axis, the lengths of the ellipsoid's axes will be:
    # a (remains the same), b, and b (the ellipsoid is symmetric around the major axis)

    # The center of the ellipsoid will be the same as the center of the ellipse, but with an added z-coordinate
    center = [center_x, center_y, 512]

    # The angle is the orientation of the major axis in the XY plane
    # Since the rotation is around the major axis, this angle also represents the orientation of the ellipsoid

    # Constructing the ellipsoid parameters
    ellipsoid_params = {
        'center': center,
        'axes_lengths': (a, b, b),
        'orientation': angle,
        'original_ellipse_angle': angle
    }

    return ellipsoid_params



def plot_sphere_and_ellipsoid(size_x, size_y, size_z, sphere_radius, sphere_center, ellipsoid_params):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    # Set the axes limits based on the provided sizes
    ax.set_xlim([0, size_x])
    ax.set_ylim([0, size_y])
    ax.set_zlim([0, size_z])

    # Plot the sphere
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x_sphere = sphere_center[0] + sphere_radius * np.sin(v) * np.cos(u)
    y_sphere = sphere_center[1] + sphere_radius * np.sin(v) * np.sin(u)
    z_sphere = sphere_center[2] + sphere_radius * np.cos(v)
    ax.plot_wireframe(x_sphere, y_sphere, z_sphere, color="r", alpha=0.5)

    # Plot the ellipsoid
    a, b, c = ellipsoid_params['axes_lengths']
    center = ellipsoid_params['center']
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x_ellipsoid = center[0] + a * np.sin(v) * np.cos(u)
    y_ellipsoid = center[1] + b * np.sin(v) * np.sin(u)
    z_ellipsoid = center[2] + c * np.cos(v)
    ax.plot_wireframe(x_ellipsoid, y_ellipsoid, z_ellipsoid, color="b", alpha=0.5)

    plt.show()



def insert_3d_fragment(data_3d, fragment_3d, insert_at_x, insert_at_y, insert_at_z):
    """
    Insert a 3D fragment into a larger 3D array at specified coordinates, with the fragment's center at the given (x, y, z) position.

    Args:
    - data_3d (np.ndarray): The larger 3D NumPy array to insert into.
    - fragment_3d (np.ndarray): The 3D NumPy array fragment to be inserted.
    - insert_at_x (int): X-coordinate for the center of the insertion in the larger array.
    - insert_at_y (int): Y-coordinate for the center of the insertion in the larger array.
    - insert_at_z (int): Z-coordinate for the center of the insertion in the larger array.

    Returns:
    - np.ndarray: The updated data_3d array with the fragment_3d inserted.
    """
    # Calculate the size of the fragment
    frag_dim_x, frag_dim_y, frag_dim_z = fragment_3d.shape

    # Calculate start and end indices for the insertion in the data_3d array
    start_x = max(insert_at_x - frag_dim_x // 2, 0)
    end_x = min(start_x + frag_dim_x, data_3d.shape[0])
    start_y = max(insert_at_y - frag_dim_y // 2, 0)
    end_y = min(start_y + frag_dim_y, data_3d.shape[1])
    start_z = max(insert_at_z - frag_dim_z // 2, 0)
    end_z = min(start_z + frag_dim_z, data_3d.shape[2])

    # Adjust the fragment if the insertion goes beyond the bounds of the data_3d array
    frag_start_x = max(0, -start_x + insert_at_x - frag_dim_x // 2)
    frag_end_x = frag_start_x + min(frag_dim_x, end_x - start_x)
    frag_start_y = max(0, -start_y + insert_at_y - frag_dim_y // 2)
    frag_end_y = frag_start_y + min(frag_dim_y, end_y - start_y)
    frag_start_z = max(0, -start_z + insert_at_z - frag_dim_z // 2)
    frag_end_z = frag_start_z + min(frag_dim_z, end_z - start_z)

    # Insert the adjusted fragment into the data_3d array
    data_3d[start_x:end_x, start_y:end_y, start_z:end_z] = fragment_3d[frag_start_x:frag_end_x, frag_start_y:frag_end_y, frag_start_z:frag_end_z]

    return data_3d


def plot_3d_array_and_sphere(data_3d, radius):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    # Plotting the 3D array (simplified as scatter points for visualization)
    x, y, z = np.where(data_3d > 0)
    ax.scatter(x, y, z)
    
    # Adding a transparent sphere
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x_sphere = radius * np.cos(u) * np.sin(v)
    y_sphere = radius * np.sin(u) * np.sin(v)
    z_sphere = radius * np.cos(v)
    ax.plot_wireframe(x_sphere, y_sphere, z_sphere, color="r", alpha=0.5)
    
    plt.show()



def ellipsoid_error_basic(params):
    # Unpack the ellipsoid parameters
    x0, y0, z0, a, b, c = params
    
    # Initialize total error
    total_error = 0
    
    # Calculate the error for each point
    for (x, y, z) in points:
        # Equation of an ellipsoid
        error = ((x - x0) / a) ** 2 + ((y - y0) / b) ** 2 + ((z - z0) / c) ** 2 - 1
        # Sum of squared errors
        total_error += error ** 2
    
    return total_error



def ellipsoid_error_round_area(params, data_3d, area_center_x, area_center_y, area_radius):
    # Unpack the ellipsoid parameters
    x0, y0, z0, a, b, c = params
    
    # Initialize total error
    total_error = 0
    
    # Calculate the error for each point within the specified area
    for (x, y, z) in data_3d:
        # Check if the point is within the specified area
        if ((x - area_center_x) ** 2 + (y - area_center_y) ** 2) <= area_radius ** 2:
            # Equation of an ellipsoid, considering only points within the area
            error = ((x - x0) / a) ** 2 + ((y - y0) / b) ** 2 + ((z - z0) / c) ** 2 - 1
            # Sum of squared errors for points within the area
            total_error += error ** 2
    
    return total_error





def find_figure_points(array3D, x_min, x_max, y_min, y_max, z_min, z_max):
    # Identify points with value > 0
    indices = np.argwhere(array3D > 0)
    
    # Filter points within the specified box
    filtered_indices = []
    for ind in indices:
        x, y, z = ind
        if x_min <= x <= x_max and y_min <= y <= y_max and z_min <= z <= z_max:
            filtered_indices.append(ind)
    
    # Extract coordinates of filtered points
    X = np.array(filtered_indices)  # Use filtered_indices directly as they are the coordinates
    return X


def ellipsoid_fit_list(X):
    
    # least squares method, 
    # approximation of ellipses by minimizing the sum of the squared 
    # differences between the observed values and the values predicted 
    # by the model

    #  Ensure X is 2D with points as rows
    X = np.atleast_2d(X)
    
    # Mean of the points for a rough center estimate
    x0 = X.mean(axis=0)
    
    if X.shape[1] != 3:
        raise ValueError("Each point in X must have 3 coordinates")
    
    # Initial parameter guess: [a, b, c, x0, y0, z0]
    p0 = np.array([1, 1, 1] + list(x0))
    
    # Objective function: distance from point to ellipsoid surface
    def residuals(p, X):
        a, b, c, x0, y0, z0 = p
        return ((X[:, 0] - x0)**2 / a**2 + 
                (X[:, 1] - y0)**2 / b**2 + 
                (X[:, 2] - z0)**2 / c**2 - 1)
    
    # Least squares fitting
    result = least_squares(residuals, p0, args=(X,))
    
    # Fitted parameters
    a, b, c, x0, y0, z0 = result.x
    return a, b, c, x0, y0, z0



def ellipsoid_fit(data_3d, center_x, center_y, center_z, radius):
       
    x_min = center_x - radius 
    x_max = center_x + radius 
    y_min = center_y - radius 
    y_max = center_y + radius 
    z_min = center_z - radius 
    z_max = center_z + radius

    
    print('Picking up the segment for ellipsoid approximation.. : ')
    figure_points =  find_figure_points(data_3d, x_min, x_max, y_min, y_max, z_min, z_max) 
    a, b, c, x0, y0, z0 = ellipsoid_fit_list(figure_points)

    ellipsoid_params = {
        'center': (x0,y0,z0),
        'axes_lengths': (a, b, c),
    }
    print('Ellipsoid fitted parameters: ')
    print('a, b, c, x0, y0, z0: ' + str(a)+', '+str(b)+', '+str(c)+', '+str(x0)+', '+str(y0)+', '+str(z0))

    return ellipsoid_params




def plot_3d_data_and_create_sphere(data_3d, radius, center_x, center_y, center_z, filename='sphere.stl', subdivisions=20):
    # Create a new figure for this plot
    fig = plt.figure(figsize=(8, 6))  # You can adjust the size if needed
    ax = fig.add_subplot(111, projection='3d')
    
    # Plotting the 3D array (simplified as scatter points for visualization)
    x_data, y_data, z_data = np.where(data_3d > 0)
    ax.scatter(x_data, y_data, z_data)
    
    # Adding a transparent sphere for visualization
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    x_sphere = center_x + radius * np.outer(np.cos(u), np.sin(v))
    y_sphere = center_y + radius * np.outer(np.sin(u), np.sin(v))
    z_sphere = center_z + radius * np.outer(np.ones(np.size(u)), np.cos(v))
    ax.plot_wireframe(x_sphere, y_sphere, z_sphere, color="r", alpha=0.5, linewidth=0.3)

    # Show the plot in a new window
    plt.show()

    # Creating a sphere mesh for STL
    phi = np.linspace(0, np.pi, subdivisions)
    theta = np.linspace(0, 2 * np.pi, subdivisions)
    phi, theta = np.meshgrid(phi, theta)
    x = center_x + radius * np.sin(phi) * np.cos(theta)
    y = center_y + radius * np.sin(phi) * np.sin(theta)
    z = center_z + radius * np.cos(phi)

    # Create the faces of the sphere
    faces = []
    for i in range(subdivisions - 1):
        for j in range(subdivisions - 1):
            faces.append([x[i][j], y[i][j], z[i][j], x[i+1][j], y[i+1][j], z[i+1][j+1], x[i+1][j+1], y[i+1][j+1], z[i+1][j+1]])  # Upper right triangle
            faces.append([x[i][j], y[i][j], z[i][j], x[i+1][j+1], y[i+1][j+1], z[i+1][j+1], x[i][j+1], y[i][j+1], z[i][j+1]])  # Lower left triangle

    # Create the mesh
    sphere_mesh = mesh.Mesh(np.zeros(len(faces), dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):
            sphere_mesh.vectors[i][j] = f[3*j:3*j+3]

    # Save the mesh to an STL file
    sphere_mesh.save(filename)



def panda3d_textured_sphere_from_array(texture_2d, texture_center_x, texture_center_y, texture_radius, sphere_radius, sphere_center, latitude, longitude):
    class MyApp(ShowBase):
        def __init__(self):
            ShowBase.__init__(self)
            
            # Extract the relevant portion of the texture_2d array
            texture_portion = texture_2d[texture_center_y-texture_radius:texture_center_y+texture_radius,
                                         texture_center_x-texture_radius:texture_center_x+texture_radius]
            
            # Convert the NumPy array to an image using PIL
            img = Image.fromarray(np.uint8(texture_portion * 255) , 'L')
            
            # Convert the PIL image to PNMImage which Panda3D can use
            pnm_image = PNMImage()
            pnm_image.read(StringStream(img.tobytes()), 'PGM')
            
            # Create a Panda3D texture from the PNMImage
            texture = Texture()
            texture.load(pnm_image)

            # Load the sphere model and set its texture
            sphere = self.loader.loadModel("models/misc/sphere")
            sphere.setTexture(texture, 1)
            sphere.setPos(sphere_center[0], sphere_center[1], sphere_center[2])
            sphere.setScale(sphere_radius, sphere_radius, sphere_radius)
            sphere.reparentTo(self.render)

            # Adjust UV mapping to position texture at the given latitude and longitude (not implemented in this example)

            # Disable default mouse-based camera control to use a fixed viewpoint
            self.disableMouse()
            self.camera.setPos(sphere_center[0], sphere_center[1] - 2 * sphere_radius, sphere_center[2])
            self.camera.lookAt(sphere)

    app = MyApp()
    app.run()




def main(fits_fname, output_stl):
    # Load background fits
    data_feature, header = load_fits(fits_fname)
    x_max, y_max = header['NAXIS1'], header['NAXIS2']
    z_max =max(x_max, y_max)
    solar_radius = header['R_SUN']
    sun_center_x = int(header['CRPIX1']/4) 
    sun_center_y=  int(header['CRPIX2']/4)
    sun_center_z =  int(z_max / 2)
    feature_center_z = int(z_max / 2)

   
    print('x_max, y_max: '+ str(x_max)+', '+str(y_max))
    print('solar_radius: '+ str(solar_radius) )
    print('sun_center_x, sun_center_y:' + str(sun_center_x)+', '+str(sun_center_y))


    feature_radius = 100

    # Load feature fits
    #feature_center = calc_mass_center(feature_data)
    feature_center_x, feature_center_y  = calc_mass_center(data_feature)
    print('feature_center_x, feature_center_y: ' + str(feature_center_x)+', '+str(feature_center_y))
    
    print('Ellipse approximation...')
    ellipse_params=approximate_ellipse_2d(data_feature)
    ell_center_x, ell_center_y = ellipse_params['center_x'], ellipse_params['center_y']
    ell_major_axis_length, ell_minor_axis_length = ellipse_params['major_axis_length'], ellipse_params['minor_axis_length']
    ell_angle = ellipse_params['angle']  # In degrees
    
    print('center_x, center_y: '+str(ell_center_x)+', '+str(ell_center_y))
    print('major_axis_length, minor_axis_length: '+ str(ell_major_axis_length)+', '+str(ell_minor_axis_length))
    A = [sun_center_x, sun_center_y, sun_center_z]  # Start point of the rotation axis
    B = [feature_center_x, feature_center_y, feature_center_z]  # End point of the rotation axis
    
    feature_crop = crop_fragment(data_feature, feature_center_x, feature_center_y, feature_radius)
    x1_c, y1_c, x2_c, y2_c = calculate_cropped_line_coordinates(sun_center_x,  sun_center_y, feature_center_x, feature_center_y, feature_center_x, feature_center_y, feature_radius)
    feature_3d = rotate_2d_fragment(feature_crop, x1_c, y1_c, x2_c, y2_c)  
    data_3d = np.zeros((x_max, y_max, z_max))
    data_3d = insert_3d_fragment(data_3d, feature_3d, feature_center_x, feature_center_y, feature_center_z) 
      
    initial_guess = ell_center_x, ell_center_y, feature_center_z, ell_major_axis_length, ell_minor_axis_length  
    ellipsoid_params=ellipsoid_fit(data_3d, feature_center_x, feature_center_y, feature_center_z, feature_radius)
   
    #plot_3d_array_and_sphere(data_3d, solar_radius)
    #plot_3d_data_and_create_sphere(data_3d, solar_radius, sun_center_x, sun_center_y, sun_center_z, filename='sphere.stl', subdivisions=20)
    
    #plot Ellipsoid_approoximated 
    plot_sphere_and_ellipsoid(x_max, y_max, z_max, solar_radius, A, ellipsoid_params)
 


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 3:
        print("Usage: python script.py <feature.fits> <output.stl>")
    else:
        main(sys.argv[1], sys.argv[2] )



